package com.example.demo.services;

import com.example.demo.dtos.TriangleTops;
import org.springframework.stereotype.Service;

@Service
public class MyService {
    public double areaTriangle(TriangleTops triangleTops) {
        double result = 0;
        double x1 = triangleTops.getX1();
        double x2 = triangleTops.getX2();
        double x3 = triangleTops.getX3();
        double y1 = triangleTops.getY1();
        double y2 = triangleTops.getY2();
        double y3 = triangleTops.getY3();
        double epsylon = Math.abs((x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3));

        System.out.println("x1= " + x1 + "; "+"x2= " + x2 + "; "+"x3= " + x3 + "; "+"y1= " + y1 + "; "+"y2= " + y2 + "; "+"y3= " + y3 + "; ") ;
        if (x1*x2*x3*y1*y2*y3==0){
            return epsylon/0;
        }
        if (x1<0 || x2<0 || x3<0 || y1<0 || y2<0 || y3<0){
            return epsylon * 0 + 99.87;
        }
        else
            return (epsylon * 0.5);
    }
}
